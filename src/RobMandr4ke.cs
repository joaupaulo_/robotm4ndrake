using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robocode;
using System.Drawing;
using Robocode.Util;
using System.Security.Cryptography.X509Certificates;

namespace Robo
{

    class RobMandr4ke : AdvancedRobot
    {
        double errei = 0;
        public override void Run()
        {
            SetColors(Color.Pink, Color.Pink, Color.Pink, Color.Pink, Color.Pink);
            while (true)
            {
                Ahead(100);
                TurnGunRight(360);
                Back(100);
                TurnGunRight(360);
            }
        }

        public override void OnScannedRobot(ScannedRobotEvent evnt)
        {
            double miraFixa = evnt.HeadingRadians + HeadingRadians;
            SetTurnGunRightRadians(Utils.NormalAbsoluteAngle(miraFixa - GunHeadingRadians));
            // O radar verifica o robô a menos de 200 pixels de distancia e desencadeia uma serie de ações .
            if (evnt.Distance < 200)
            {
                // se existir com pouquissima energia o robô segue até ele e executa um tiro de nivel 2 !
                if( evnt.Energy < 10 )
                {
                    SetAhead(evnt.Distance);
                    Fire(2);
                }

                // Caso a energia e a velocidade do meu robô seja superior ele irá se aproximar ao robô adversário e efetuar disparos de nivel 3 !
                if (Energy > evnt.Energy && Velocity > evnt.Velocity)
                {
                    SetAhead(evnt.Distance / 2); ;
                    Fire(3);
                }
                // Caso a energia e a velocidade do meu robô sejam semelhantes ele iria se aproximar e efetuar disparos de nivel 3!
                else if (Energy == evnt.Energy && Velocity == evnt.Velocity)
                {
                    SetAhead(evnt.Distance / 2);
                    Fire(3);
                }

                // Caso a energia do meu robô for inferior ele irá se afastar e efetuar disparos de nivel 2 ! 
                else if (Energy < evnt.Energy && Velocity < evnt.Velocity)
                {
                    Fire(2);
                    Back(90 - evnt.Distance);
                }
                else // Caso ela não satifasa nenuma dessas condições ele ira efetuar disparos nivel 3 !
                {
                    Fire(3);
                }
            }
            // Caso o radar tenha verificado um robô adversário entre 200 e 300 pixels ele irá desecadeiar uma serie de ações ! 
            if (evnt.Distance > 200 && evnt.Distance < 300)
            {
                // Caso a energia e a velocidade do meu robô seja superior ele irá  se aproximar ao robô adversário e efetuar disparos de nivel 2 ! 
                if (Energy > evnt.Energy && Velocity > evnt.Velocity)
                {
                    SetAhead(evnt.Distance);
                    Fire(2);
                }
                // Caso a energia e a velocidade do meu robô sejam semelhantes ele irá se aproximar e efetuar disparos de nivel 2 !
                else if (Energy == evnt.Energy && Velocity == evnt.Velocity)
                {
                    SetAhead(evnt.Distance / 2);
                    Fire(2);
                }
                // Caso a energia do meu robô for inferior ele irá se afastar e efetuar disparos de nivel 2 ! 
                else if (Energy < evnt.Energy && Velocity < evnt.Velocity)
                {
                    Fire(2);
                    Back(90 - evnt.Distance);
                }
                else
                // Caso ela não satifasa nenuma dessas condições ele irá efetuar disparos nivel 3 !
                {
                    Fire(2);
                }
            }
            // O radar verifica o robô a mais de 200 e menos de 400 pixel de distancia e irá realiza uma serie de funções !
            if (evnt.Distance > 300 && evnt.Distance < 400)
            {
                Fire(2);
            }
            // O radar verifica um robô a mais de 400 pixels e desencadeia um serie de ações ! 
            if (evnt.Distance > 400)
            {
                // Para evitar o desperdicio de bala o robô,caso mais de 3 munições colidam contra a parede o robô ira executar o tiro entre os angulos definidos abaixo ! 
                if(errei > 3 )
                {
                    if (evnt.Bearing > -10 && evnt.Bearing < 10)
                    {
                        Fire(3);
                    } 
                    else
                    {
                        Fire(1);
                    }
                }
            }
        }



        public override void OnHitRobot(HitRobotEvent evnt)
        {
            // Ao acontecer a colisão caso o robô adversário tenha energia inferior a minha eu realizo tiros de nivel 3 !
            if (Energy > evnt.Energy)
            {
                TurnRight(evnt.Bearing);
                Fire(3);
            }
            // Ao acontecer a colisão caso o robô adversário tenha energia semelhante a minha eu realializo tiros nivel 3 !
            else if (Energy == evnt.Energy)
            {
                TurnRight(evnt.Bearing);
                Fire(3);
            }
            // Ao acontecer a colisão caso o robô adversário tenha energia inferior a minha eu realizado tiros nivel 2 e me afasto!
            else if (Energy < evnt.Energy)
            {
                TurnRight(evnt.Bearing);
                Fire(2);    
            }
            // Caso ela não satifasa nenuma dessas condições ele ira efetuar disparos nivel 3 !
            else
            {
                TurnRight(evnt.Bearing);
                Fire(3);
            }
        }


        public override void OnBulletMissed(BulletMissedEvent evnt)
        {
            //contabiliza o número de balas que colidiram contra a parede !
            errei++;
        }


        public override void OnHitByBullet(HitByBulletEvent evnt)
        {
            //Caso o robô seja atigindo por uma bala ele desvia contando com um o angulo da bala com a qual foi atingido !
            TurnLeft(90 - evnt.Bearing);
        }


        public override void OnWin(WinEvent evnt)
        {
            dancaVitoria();
        }

        public void dancaVitoria()
        {
            for (int i = 0; i < 50; i++)
            {
                TurnRight(30);
                TurnLeft(30);
            }
        }
    }
}
